# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-25 00:48+0000\n"
"PO-Revision-Date: 2022-08-27 04:10+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: notifier/DiscoverNotifier.cpp:144
#, kde-format
msgid "View Updates"
msgstr "განახლებების ნახვა"

#: notifier/DiscoverNotifier.cpp:256
#, kde-format
msgid "Security updates available"
msgstr "ხელმისაწვდომია უსაფრთოხების განახლებები"

#: notifier/DiscoverNotifier.cpp:258
#, kde-format
msgid "Updates available"
msgstr "ხელმისაწვდომია განახლება"

#: notifier/DiscoverNotifier.cpp:260
#, kde-format
msgid "System up to date"
msgstr "სისტემა განახლებულია"

#: notifier/DiscoverNotifier.cpp:262
#, kde-format
msgid "Computer needs to restart"
msgstr "საჭიროა კომპიუტერის რესტარტი"

#: notifier/DiscoverNotifier.cpp:264
#, kde-format
msgid "Offline"
msgstr "გათიშული"

#: notifier/DiscoverNotifier.cpp:266
#, kde-format
msgid "Applying unattended updates…"
msgstr "განახლებების გადატარება…"

#: notifier/DiscoverNotifier.cpp:300
#, kde-format
msgid "Restart is required"
msgstr "საჭიროა რესტარტი"

#: notifier/DiscoverNotifier.cpp:301
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr "განახლებების ძალაში შესასვლელად საჭიროა მოწყობილობის გადატვირთვა."

#: notifier/DiscoverNotifier.cpp:307
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "რესტარტი"

#: notifier/DiscoverNotifier.cpp:327
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "განახლება"

#: notifier/DiscoverNotifier.cpp:328
#, kde-format
msgid "Upgrade available"
msgstr "განახლება ხელმისაწვდომია"

#: notifier/DiscoverNotifier.cpp:329
#, kde-format
msgid "New version: %1"
msgstr "ახალი ვერსია: %1"

#: notifier/main.cpp:39
#, kde-format
msgid "Discover Notifier"
msgstr "Discover-ის გამფრთხილებელი"

#: notifier/main.cpp:41
#, kde-format
msgid "System update status notifier"
msgstr "სისტემის განახლების მდგომარეობის გამფრთხილებელი"

#: notifier/main.cpp:43
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2022 Plasma -ის პროგრამისტების გუნდი"

#: notifier/main.cpp:47
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Temuri Doghonadze"

#: notifier/main.cpp:47
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: notifier/main.cpp:52
#, kde-format
msgid "Replace an existing instance"
msgstr "გაშვებული ასლის ჩანაცვლება"

#: notifier/main.cpp:54
#, kde-format
msgid "Do not show the notifier"
msgstr "გამფრთხილებელი აღარ მაჩვენო"

#: notifier/main.cpp:54
#, kde-format
msgid "hidden"
msgstr "დამალული"

#: notifier/NotifierItem.cpp:35 notifier/NotifierItem.cpp:36
#, kde-format
msgid "Updates"
msgstr "განახლებები"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Open Discover…"
msgstr "Discover-ის …"

#: notifier/NotifierItem.cpp:55
#, kde-format
msgid "See Updates…"
msgstr "განახლებების ნახვა…"

#: notifier/NotifierItem.cpp:60
#, kde-format
msgid "Refresh…"
msgstr "განახლება…"

#: notifier/NotifierItem.cpp:64
#, kde-format
msgid "Restart to apply installed updates"
msgstr "განახლებების გადასატარებლად გადატვირთეთ კომპიუტერი"

#: notifier/NotifierItem.cpp:65
#, kde-format
msgid "Click to restart the device"
msgstr "მოწყობილობის გადასატვირთად დააწკაპუნეთ"
