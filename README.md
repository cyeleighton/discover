[Discover](https://apps.kde.org/discover/)
helps you find and install applications, games, and tools.
You can search or browse by category,
review screen shots like the one below
and read reviews to help you pick the perfect app.

![Discover screen shot](https://cdn.kde.org/screenshots/plasma-discover/plasma-discover.png)

With [Discover](https://apps.kde.org/discover/),
you can manage software from multiple sources,
including your operating system&rsquo;s software repository,
[Flatpak](https://www.flatpak.org/) repositories,
the [Snap](https://www.snapcraft.io) store, or even [AppImages](https://store.kde.org/).

Finally,
[Discover](https://apps.kde.org/discover/)
also allows you to find, install and manage add-ons for Plasma
and all your favorite applications!
